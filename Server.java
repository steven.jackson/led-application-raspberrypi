import java.net.*;
import java.io.*;

public class Server implements Runnable {
	private Socket socket;
	private ServerSocket server;
	private DataInputStream in;
	public int port;
	public boolean hasConnection;

	public Server(int port) {
		this.port = port;
		hasConnection = false;
	}

	@Override
	public void run() {
		try {
			server = new ServerSocket(port);
			System.out.println("Server started");

			System.out.println("Waiting for a client ...");

			socket = server.accept();
			hasConnection = true;
			System.out.println("Client accepted");

			in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		}
		catch (IOException i) {
			System.out.println(i);
		}
	}

	public String readFromClient(){
		String line = "none";
		try {
			line = in.readUTF();
		}
		catch(EOFException e){
			//Do nothing
		}
		catch(IOException i) {
			System.out.println(i);
			System.out.println("Lost connection to client...\nAttempting to reconnect...");
			try{
				socket = server.accept();
				System.out.println("Client accepted");
				in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
				line = "none";
			}
			catch(IOException ii){
				System.out.println(i);
			}
		}
		return line;
		
	}

	public void closeConnection(){
		try {
			socket.close();
			in.close();
		}
		catch(IOException i) {
			System.out.println(i);
		}
	}

	public boolean isConnected(){
		return hasConnection;
	}
}
