import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.wiringpi.SoftPwm;

public class LED {
	//The LED gpio
	private GpioPinDigitalOutput ledPin;
	private int pin;

	public LED() {
		GpioController gpio = GpioFactory.getInstance();
		ledPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, "LED", PinState.LOW);
		
	}

	public LED(int pin){
		this.pin = pin;
		SoftPwm.softPwmCreate(this.pin, 100, 100);
	}

	public static void init(){
		GpioController gpio = GpioFactory.getInstance();
	}

	public void setValue(int value){
		SoftPwm.softPwmWrite(this.pin, value);
	}

	public static void setupPWM(){
		//SoftPwm.wiringPiSetup();
	}

	public void on(){
		ledPin.high();
	}

	public void off(){
		ledPin.low();
	}
}
