import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.io.*;

public class LEDController {
	public LED pin17Red;
	public LED pin27Green;
	public LED pin22Yellow;

	public static void main(String [] args) throws IOException, InterruptedException {
		LEDController lc = new LEDController();

		Server server = new Server(40001);
		Thread serverThread = new Thread(server);
		serverThread.start();
		
		while(!server.isConnected()) Thread.sleep(100);

		Command c = new Command();
		while(true){
			if(c.isValid){
				lc.pin17Red.setValue(Integer.parseInt(c.redValue));
				System.out.println("Updating Red To: " + c.redValue);
				lc.pin27Green.setValue(Integer.parseInt(c.greenValue));
				System.out.println("Updating Green To: " + c.greenValue);
				lc.pin22Yellow.setValue(Integer.parseInt(c.yellowValue));
				System.out.println("Updating Yellow To: " + c.yellowValue);
				c.isValid = false;
			}
			c.parseInput(server.readFromClient());
		}
	}

	public LEDController() {
		//pin17Red = new LED();
		LED.init();
		//LED.setupPWM();
		pin17Red = new LED(0);
		pin17Red.setValue(100);
		System.out.println("Updating Red To: 100");
		pin27Green = new LED(2);
		pin27Green.setValue(100);
		System.out.println("Updating Green To: 100");
		pin22Yellow = new LED(3);
	}

	private static class Command{
		boolean isValid;
		String redValue;
		String greenValue;
		String yellowValue;
			
		public Command(){
			this.isValid = false;
		}
			
		public Command(String red, String green, String yellow){
			isValid = true;
			this.redValue = red;
			this.greenValue = green;
			this.yellowValue = yellow;
		}
			
		public void parseInput(String command){
			if(!command.equals("none")){
				System.out.println("Read From Client: " + command);
				String delims = "[ ]";
				String[] tokens = command.split(delims);
				this.isValid = true;
				this.redValue = tokens[0];
				this.greenValue = tokens[1];
				this.yellowValue = tokens[2];
			}
			
		}
	}
}

